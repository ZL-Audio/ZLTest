#include "main_panel.h"

namespace zlpanel {
    MainPanel::MainPanel(PluginProcessor &p) {
        setSize(200, 200);
    }

    MainPanel::~MainPanel() {
    }

    void MainPanel::paint(juce::Graphics &g) {
        g.fillAll(juce::Colours::darkred);
    }

    void MainPanel::resized() {}
} // panel