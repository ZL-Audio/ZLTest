#ifndef ZLTEST_MAIN_PANEL_H
#define ZLTEST_MAIN_PANEL_H

#include <juce_audio_processors/juce_audio_processors.h>
#include <juce_opengl/juce_opengl.h>

#include "PluginProcessor.h"

namespace zlpanel {

    class MainPanel : public juce::Component {
    public:
        explicit MainPanel(PluginProcessor &p);

        ~MainPanel() override;

        void paint(juce::Graphics &g) override;

        void resized() override;

    private:
        juce::OpenGLContext openGLContext;
        
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainPanel)
    };

} // panel
#endif